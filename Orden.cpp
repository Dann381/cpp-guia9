#include <iostream>
using namespace std;
#include "Orden.h"

Orden::Orden(){
    string opt;
}

Orden::Orden(string opt){
    this->opt = opt;
}

//Calcula la key del dato ingresado usando mod
int Orden::hash(int n, int dato){
    int D = (dato % n) + 1;
    return D;
}

//Arregla la colisión usando el método de prueba lineal
void Orden::prueba_lineal(int n, int dato, int* claves){
    int inicio = (dato % n);
    int pos = inicio + 1;
    while((pos < n) && (claves[pos] != -1)){
        pos = pos + 1;
        if (pos >= n){
            pos = 0;
        }
    }
    claves[pos] = dato;
    //Muestra a qué posición se movió
    cout << "Moviendo a la posición: " << pos << endl; 
}

//Arregla la colisión usando el método de prueba cuadrática
void Orden::prueba_cuadratica(int n, int dato, int* claves){
    int inicio = (dato % n);
    int pos = inicio + 1;
    int i = 1;
    while((pos < n) && (claves[pos] != -1)){
        pos = pos + (i * i);
        if (pos >= n){
            i = 1;
            pos = inicio + 1;

        }
        i++;
    }
    claves[pos] = dato;
    cout << "Moviendo a la posición: " << pos << endl;
}

//Arregla la colisión usando el método de doble direccion hash
void Orden::doble_hash(int n, int dato, int* claves){
    int D = hash(n, dato);
    D = hash(n, D);
    while((D < n) && (claves[D] != -1)){
        D = hash(n, D);
    }
    claves[D] = dato;
    cout << "Moviendo a la posición: " << D << endl; 
}

//Funcion que sirve como main para elegir qué método usar según la opción del usuario
void Orden::ordenar(int n, int dato, int* claves){
    if(this->opt == "L"){
        prueba_lineal(n, dato, claves);
    }
    else if(this->opt == "C"){
        prueba_cuadratica(n, dato, claves);
    }
    else if(this->opt == "D"){
        doble_hash(n, dato, claves);
    }
    else if(this->opt == "E"){
        cout << "Función fuera de servicio, disculpe las molestias" << endl;
        exit(1);
    }
}