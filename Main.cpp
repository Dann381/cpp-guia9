#include <iostream>
using namespace std;
#include "Orden.h"

//Ingresa un dato al arreglo, e indica si hay una colisión.
void funcion_hash(int n, int dato, int* claves, Orden orden){
    int temp;
    temp = (dato % n);
    system("clear");
    if (claves[temp] == -1){
        claves[temp] = dato;
    }
    else {
        //Si hay una colisión se ordena según el método que ingresó el usuario (L, C, D, o E)
        cout << "Colisión en la posición: " << temp+1 << endl;
        orden.ordenar(n, dato, claves);
    }
}

//Funcion que busca si hay un dato en el arreglo o no
void buscar(int n, int dato, int* claves){
    system("clear");
    int flag = 0;
    for(int i = 0; i < n; i++){
        if(claves[i] == dato){
            flag = 1;
            //Muestra la posición del dato encontrado
            cout << "El dato se encuentra en la posición: " << i << endl;
        }
    }
    if(flag == 0){
        //El dato no se encontró en el arreglo
        cout << "El dato no se encuentra" << endl;
    }
}

int main(int argc, char **argv){
    srand(time(NULL));
    system("clear");
    int n = 20; //Tamaño del arreglo fijo en 20
    string opt = "aaa";
    //Lectura del parámetro ingresado por el usuario
    if(argv[1]){
        opt = argv[1]; 
    }

    //No alcancé a realizar el método de Encadenamiento, por eso lo quité
    if(opt == "E"){
        system("clear");
        cout << "Función fuera de servicio, disculpe las molestias" << endl;
        exit(1);
    }
    //Solo se continúa con el algoritmo si el usuario ingresa las letras L, D o C mayúsculas
    else if(opt != "L" && opt != "C" && opt != "D"){
        system("clear");
        cout << "Por favor ingrese un parámetro válido" << endl;
        exit(1);
    }

    //Se crea la clase que contiene los métodos para reordenar las colisiones
    Orden orden = Orden(opt); 
    int *claves;
    claves = new int[n];

    for(int i = 0; i < n; i++){
        claves[i] = -1;
    } 
    int dato;
    int salir;
    int contador = 0;
    //Se ingresa al menú
    while (salir != 4){
        cout << "-------------------- Menú --------------------" << endl;
        cout << "[1] Ingresar dato" << endl;
        cout << "[2] Buscar dato" << endl;
        cout << "[3] Mostrar datos" << endl;
        cout << "[4] Salir" << endl;
        cout << "Opción: ";
        cin >> salir;
        if(salir == 1){
            //El contador limita la cantidad de datos a agregar (menor al tamaño del arreglo que es 20)
            if(contador >= n){
                system("clear");
                cout << "No se pueden agregar más datos" << endl;
            }
            else {
                //Se ingresa un entero
                cout << "Ingrese un entero: ";
                cin >> dato;
                //La función Hash lo agrega al arreglo y soluciona las colisiones
                funcion_hash(n, dato, claves, orden);
                contador++;
                //Se muestran los datos despues de agregarlos
                cout << "Datos: ";
                for(int i = 0; i<n; i++){
                    cout << claves[i] << " | ";
                }
                cout << endl;
                }
        }
        else if(salir == 2){
            //Busca un dato ingresado por el usuario
            cout << "Ingrese un dato para buscar: ";
            cin >> dato;
            buscar(n, dato, claves);
        }
        else if(salir == 3){
            //Muestra todos los datos y sus índices
            system("clear");
            cout << "Mostrando datos" << endl;
            cout << "Posicion   |   Dato" << endl;            
            for(int i = 0; i<n; i++){
                    cout << i << "   |   " << claves[i] << endl;
                }
                cout << endl;
        }
        else if(salir == 4){
            //Sale del algoritmo
            system("clear");
            cout << "Algoritmo terminado" << endl;
        }
        else{
            system("clear");
            cout << "Ingrese una opción válida" << endl;
        }
    }   
    //registros = {23, 42, 5, 66, 14, 43, 59, 81, 37, 49, 28, 55, 94, 80, 64};

    return 0;
}