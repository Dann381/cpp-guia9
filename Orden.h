#ifndef ORDEN_H
#define ORDEN_H

class Orden{
    private:
        string opt;
    public:
        Orden();
        Orden(string opt);
        int hash(int n, int dato);
        void prueba_lineal(int n, int dato, int* registros);
        void prueba_cuadratica(int n, int dato, int* registros);
        void ordenar(int n, int dato, int* registros);
        void doble_hash(int n, int dato, int* claves);
};
#endif