# Guía 9 - Unidad III

### Descripción
Este algoritmo consiste en la implementación de un método Hash para agregar 20 datos a elección del usuario, y cuatro métodos de solución de colisiones en caso de que ocurran al momento de ingresar los datos con el método Hash. Los métodos para solucionar las colisiones utilizados son: Reasignación Prueba Lineal, Reasignación Prueba Cuadrática, Reasignación Doble Dirección Hash, y Encadenamiento.  

### Ejecución
Luego de descargar los archivos contenidos en este repositorio, el usuario debe escribir vía terminal el comando "make" para compilar los archivos. Esto dejará un ejecutable llamado "hash", el cual recibe un parámetro inicial obligatorio de tipo string, para elegir un método de solución de colisiones. (Ejemplo: ./hash L). El usuario tiene 4 opciones para ingresar.
- Reasignación Prueba Lineal: ./hash L
- Reasignación Prueba Cuadrática: ./hash C
- Reasignación Doble Dirección Hash: ./hash D
- Encadenamiento: ./hash E (esta opción está deshabilitada)


### Luego de ejecutar
Si se ha ejecutado correctamente, se mostrará vía terminal un menú básico con las opciones de Agregar Datos, Buscar Datos, Mostrar los Datos y Salir del algoritmo. Cada vez que el usuario ingrese un dato, se mostrará el contenido del arreglo. El número -1 en el arreglo significa que el usuario no ha ingresado ningún dato en esa posiciín. El usuario tiene un límite para agregar datos (no puede agregar más datos que el límite del arreglo), por lo que finalmente solo podrá buscar datos, ver el contenido del arreglo o salir del algorimto.

### Construido con
El presente proyecto de programación, se construyó en base al lenguaje de programación C++, en conjunto con el IDE "Visual Studio Code".

* [C++] - Lenguaje de programación utilizado.
* [Visual Studio Code](https://code.visualstudio.com/) - IDE utilizado.

### Autores
* **Daniel Tobar** - Estudiante de Ingeniería Civil en Bioinformática.
